Casa Serena’s staff consists of trained professionals in the treatment of substance abuse. The clinical and program staff have experience in evidence-based treatment and 12-Step recovery, a social model for treatment of addiction and addictive behaviors. Call (805) 966-1260 for more information!

Address: 1515 Bath St, Santa Barbara, CA 93101, USA

Phone: 805-966-1260
